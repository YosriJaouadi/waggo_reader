﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Waggo_Reader
{
    class Enregistrer
    {
        public static void run_enreg(int i, DateTime d)
        {
            try
            {
                // DateTime d = DateTime.Now;
                MySqlConnection con = DBconnect.con1();
                con.Open();
                string query = "insert into carte(time,capteur1,capteur4,capteur5,capteur6,capteur7,capteur8,capteur9,capteur10,capteur11,capteur12,capteur13,capteur14,capteur15,capteur16,capteur17,capteur18,capteur19,capteur20,capteur21,capteur22,capteur23,capteur24,capteur25,capteur26,capteur27,capteur28,capteur29,capteur30,capteur31,capteur32,plage) values('" + d.Year + "-" + d.Month + "-" + d.Day + " " + d.Hour + ":" + d.Minute + ":" + d.Second + "','" + i + "',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0)";
                MySqlCommand cm = new MySqlCommand(query, con);
                cm.ExecuteNonQuery();
                con.Close();

                MySqlConnection con3 = DBconnect.con3();
                try
                {

                    con3.Open();
                    string request = "insert into capteur1(time,valeurs) values('" + d.Year + "-" + d.Month + "-" + d.Day + " " + d.Hour + ":" + d.Minute + ":" + d.Second + "','" + i + "')";
                    MySqlCommand cmd2 = new MySqlCommand(request, con3);
                    cmd2.ExecuteNonQuery();
                    con3.Close();
                }
                catch (Exception exe) { con3.Close(); }
            }
            catch (Exception ex) { MessageBox.Show(ex.ToString()); }
        }

        public static void enregistrer_trame(string text, DateTime d)
        {
            bool b = false;
            int i = 0;
            int[] j = new int[20];
            string[] IntValuesSplit = text.Split(' ');
            foreach (String var in IntValuesSplit)
            {
                if (var != "")
                {
                    b = true;
                    try
                    {
                        j[i] = Convert.ToInt16(var);
                    }
                    catch (Exception ex) { }
                    i++;
                }
            }
            if (b == true)
            {

                MySqlConnection con = DBconnect.con1();
                con.Open();
                string query = "insert into carte(time,capteur1,capteur4,capteur2,capteur3,capteur5,capteur6,capteur7,capteur8,capteur9,capteur10,capteur11,capteur12,capteur13,capteur14,capteur15,capteur16,capteur17,capteur18,capteur19,capteur20,capteur21,capteur22,capteur23,capteur24,capteur25,capteur26,capteur27,capteur28,capteur29,capteur30,capteur31,capteur32,plage) values('" + d.Year + "-" + d.Month + "-" + d.Day + " " + d.Hour + ":" + d.Minute + ":" + d.Second + "'," + j[0] + "," + j[1] + ",0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0)";
                //string query = "delete from carte where time>'2012-06-26 10:00:00'";
                MySqlCommand cm = new MySqlCommand(query, con);
                cm.ExecuteNonQuery();
                con.Close();
            }
            b = false;
        }

        public static void enregistrer2(int a1, int a2, DateTime d)
        {

            try
            {

                MySqlConnection con = DBconnect.con1();
                con.Open();
                string query = "insert into carte(time,capteur1,capteur2,plage) values('" + d.Year + "-" + d.Month + "-" + d.Day + " " + d.Hour + ":" + d.Minute + ":" + d.Second + "','" + a1 + "','" + a2 + "',0)";
                MySqlCommand cm = new MySqlCommand(query, con);
                cm.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception ex) { MessageBox.Show(ex.ToString()); }
        }

        public static void enreg_val(int[] en, DateTime d)
        {
            MySqlConnection con = DBconnect.con1();
            try
            {


                con.Open();
                string query = "insert into carte(time,capteur1,capteur2,capteur3,capteur4,capteur5,capteur6,capteur7,capteur8,capteur9,capteur10,capteur11,capteur12,capteur13,capteur14,capteur15,capteur16,capteur17,capteur18,capteur19,plage) values('" + d.Year + "-" + d.Month + "-" + d.Day + " " + d.Hour + ":" + d.Minute + ":" + d.Second + "','" + en[0] + "','" + en[1] + "','" + en[2] + "','" + en[3] + "','" + en[4] + "','" + en[5] + "','" + en[6] + "','" + en[7] + "','" + en[8] + "','" + en[9] + "','" + en[10] + "','" + en[11] + "','" + en[12] + "','" + en[13] + "','" + en[14] + "','" + en[15] + "','" + en[16] + "','" + en[17] + "','" + en[18] + "',0)";
                MySqlCommand cm = new MySqlCommand(query, con);
                cm.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception ex)
            {
                con.Close();
                MessageBox.Show(ex.ToString());
            }
        }

        public static void enreg_cum(int[] en, DateTime d, int n)
        {
            try
            {

                MySqlConnection con = DBconnect.con1();
                con.Open();
                string query = "insert into carte(time,capteur1,capteur2,capteur3,capteur4,capteu5,capteur6,plage) values('" + d.Year + "-" + d.Month + "-" + d.Day + " " + d.Hour + ":" + d.Minute + ":" + d.Second + "','" + en[0] + "','" + en[1] + "','" + en[2] + "','" + en[3] + "','" + en[4] + "','" + en[5] + "',0)";
                MySqlCommand cm = new MySqlCommand(query, con);
                cm.ExecuteNonQuery();
                for (int i = n; i <= 6; i++)
                {
                    query = "update carte  set capteur" + i + "=0 where time='" + d.Year + "-" + d.Month + "-" + d.Day + " " + d.Hour + ":" + d.Minute + ":" + d.Second + "'";
                }
                con.Close();
            }
            catch (Exception ex) { MessageBox.Show(ex.ToString()); }
        }
    }
}
