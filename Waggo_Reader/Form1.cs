﻿/******************************Med Yosri JAOUADI****************************************/
using Modbus;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Waggo_Reader
{
    public partial class Form1 : Form
    {
        SerialPort COM1, COM2;
        ModbusMaster mm;
        List<ModbusMaster> Lmm;
        List<ModbusIPConfigurator.MODBUSTCP> Lmd;
        byte slave_id;
        public DateTime d;
        bool bcum;
        bool bcum2;
        int[] enreg;
        ushort[] regResult;
        int i;
        public Form1()
        {
            InitializeComponent();
            COM1 = new SerialPort("COM3", 1200, Parity.None, 8, StopBits.One);
            COM2 = new SerialPort("COM4", 1200, Parity.None, 8, StopBits.One);
            d = DateTime.Now;
            enreg = new int[20];
            bcum = false;
            bcum2 = false;
            Lmm = new List<ModbusMaster>();
            for (int i = 0; i < 20; i++)
            {
                enreg[i] = 0;
            }

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                Lire();
            }catch(Exception exe)
            {
                MessageBox.Show(exe.ToString());
            }
        }
        delegate void SetTextCallback(string text);

        private void Lire()
        {

            d = DateTime.Now;
#if RS485
            try
            {
                COM1.Open();
            }
            catch (Exception exep) { }


            try
            {

                if (bcum == true)
                {
                    int j = 0;
                    byte[] bb = { 0x26 };
                    while (j < i)
                    {
                        bb[0]++;
                        j++;
                    }
                    for (j = i; j < 17; j++)
                    {
                        bb[0]++;
                        COM1.Write(bb, 0, 1);
                        Thread.Sleep(250);
                        //string text = COM1.ReadExisting();
                    }
                }
                bcum = false;
            }
            catch (Exception exe) { }
#endif


            for (int j = 0; j < 20; j++)
            {
                enreg[j] = 0;
            }
            i = 0;

#if RS485


            string[] t = new string[10];

            byte[] b = { 0x1F };
            try
            {
                COM1.Write(b, 0, 1);
                Thread.Sleep(200);
                COM1.ReadExisting();
            }
            catch (Exception exe)
            {
            }

            for (i = 0; i < 8; i++)
            {
                try
                {
                    COM1.ReadExisting();
                    Thread.Sleep(25);
                    b[0]++;
                    COM1.Write(b, 0, 1);
                    Thread.Sleep(200);


                    {

                        enreg[i] = Convert.ToInt32(COM1.ReadExisting());
                    }

                }
                catch (Exception exe)
                {
                    this.notifyIcon1.BalloonTipIcon = ToolTipIcon.Error;
                    this.notifyIcon1.BalloonTipTitle = "erreur produite";
                    this.notifyIcon1.BalloonTipText = "Une erreur est survenue dans la connection avec la machine" + (i + 1) + "";
                    this.notifyIcon1.ShowBalloonTip(0);
                }
            }
            try
            {
                COM1.Close();
            }
            catch (Exception exc)
            {
            }

            Thread.Sleep(500);
            try
            {

                COM2.Open();
                Thread.Sleep(20);
                COM2.Write(b, 0, 1);
                Thread.Sleep(200);
                COM2.ReadExisting();
            }
            catch (Exception exe)
            {
            }

            for (i = 0; i < 7; i++)
            {
                try
                {
                    COM2.ReadExisting();
                    Thread.Sleep(25);
                    b[0]++;
                    COM2.Write(b, 0, 1);
                    Thread.Sleep(200);


                    {

                        enreg[i + 8] = Convert.ToInt32(COM2.ReadExisting());
                    }

                }
                catch (Exception exe)
                {
                    this.notifyIcon1.BalloonTipIcon = ToolTipIcon.Error;
                    this.notifyIcon1.BalloonTipTitle = "erreur produite";
                    this.notifyIcon1.BalloonTipText = "Une erreur est survenue dans la connection avec la machine" + (i + 9) + "";
                    this.notifyIcon1.ShowBalloonTip(0);
                }
            }
            try
            {
                COM2.Close();
            }
            catch (Exception exe)
            {
            }
#endif


            //add MODBUS CLIENT
            //lecture des compteurs
            if (Lmm[0].Connected)
            {
                try
                {
                    //Lmm[0].Connect();

                    for (int k = 0; k < 5; k++)
                    {
                        regResult = Lmm[0].ReadHoldingRegisters
                       (
                       this.slave_id,
                       (ushort)(2 * k + 1),
                       1);

                        enreg[k] = regResult[0];
                    }
                    //modifaication des registres de status //initialization des compteurs

                    for (int k = 0; k < 5; k++)
                    {
                        Lmm[0].WriteSingleRegister(this.slave_id, (ushort)(2 * k), 0x20);
                        Thread.Sleep(5);
                        Lmm[0].WriteSingleRegister(this.slave_id, (ushort)(2 * k + 1), 0);//reset counting
                        Thread.Sleep(5);
                        Lmm[0].WriteSingleRegister(this.slave_id, (ushort)(2 * k), 0);//libérer le comptage
                        Thread.Sleep(5);
                    }
                    //Lmm[0].Disconnect();
                }
                catch(Exception exe)
                {
                    this.notifyIcon1.BalloonTipIcon = ToolTipIcon.Error;
                    this.notifyIcon1.BalloonTipTitle = "erreur produite";
                    this.notifyIcon1.BalloonTipText = "WAGGO module avec ip="+Lmd[0].ip+" non connecté";
                    this.notifyIcon1.ShowBalloonTip(0);
                    
                }


               
            }
            else
            {
                this.notifyIcon1.BalloonTipIcon = ToolTipIcon.Error;
                this.notifyIcon1.BalloonTipTitle = "erreur produite";
                this.notifyIcon1.BalloonTipText = "WAGGO module avec ip=" + Lmd[1].ip + " non connecté";
                this.notifyIcon1.ShowBalloonTip(0);
            }

            //deusième waggo
            if (Lmm[1].Connected)
            {
                try
                {
                    //Lmm[1].Connect();

                    for (int k = 0; k < 2; k++)
                    {
                        regResult = Lmm[1].ReadHoldingRegisters
                       (
                       this.slave_id,
                       (ushort)(2 * k + 1),
                       1);

                        enreg[5 + k] = regResult[0];
                    }
                    //modifaication des registres de status //initialization des compteurs

                    for (int k = 0; k < 2; k++)
                    {
                        Lmm[1].WriteSingleRegister(this.slave_id, (ushort)(2 * k), 0x20);
                        Thread.Sleep(5);
                        Lmm[1].WriteSingleRegister(this.slave_id, (ushort)(2 * k + 1), 0);//reset counting
                        Thread.Sleep(5);
                        Lmm[1].WriteSingleRegister(this.slave_id, (ushort)(2 * k), 0);//libérer le comptage
                        Thread.Sleep(5);
                    }
                    //Lmm[1].Disconnect();
                }
                catch (Exception exe)
                {
                    this.notifyIcon1.BalloonTipIcon = ToolTipIcon.Error;
                    this.notifyIcon1.BalloonTipTitle = "erreur produite";
                    this.notifyIcon1.BalloonTipText = "WAGGO module avec ip=" + Lmd[1].ip + " non connecté";
                    this.notifyIcon1.ShowBalloonTip(0);
                }


                
            }
            else
            {
                this.notifyIcon1.BalloonTipIcon = ToolTipIcon.Error;
                this.notifyIcon1.BalloonTipTitle = "erreur produite";
                this.notifyIcon1.BalloonTipText = "WAGGO module avec ip=" + Lmd[1].ip + " non connecté";
                this.notifyIcon1.ShowBalloonTip(0);
            }

            //troisième waggo

            if (Lmm[2].Connected)
            {
                try
                {
                    //Lmm[2].Connect();

                    for (int k = 0; k < 4; k++)
                    {
                        regResult = Lmm[2].ReadHoldingRegisters
                       (
                       this.slave_id,
                       (ushort)(2 * k + 1),
                       1);

                        enreg[7 + k] = regResult[0];
                    }
                    //modifaication des registres de status //initialization des compteurs

                    for (int k = 0; k < 4; k++)
                    {
                        Lmm[2].WriteSingleRegister(this.slave_id, (ushort)(2 * k), 0x20);
                        Thread.Sleep(5);
                        Lmm[2].WriteSingleRegister(this.slave_id, (ushort)(2 * k + 1), 0);//reset counting
                        Thread.Sleep(5);
                        Lmm[2].WriteSingleRegister(this.slave_id, (ushort)(2 * k), 0);//libérer le comptage
                        Thread.Sleep(5);
                    }
                //    Lmm[2].Disconnect();

                }
                catch (Exception exe)
                {
                    this.notifyIcon1.BalloonTipIcon = ToolTipIcon.Error;
                    this.notifyIcon1.BalloonTipTitle = "erreur produite";
                    this.notifyIcon1.BalloonTipText = "WAGGO module avec ip=" + Lmd[2].ip + " non connecté";
                    this.notifyIcon1.ShowBalloonTip(0);
                }


                
            }
            else
            {
                this.notifyIcon1.BalloonTipIcon = ToolTipIcon.Error;
                this.notifyIcon1.BalloonTipTitle = "erreur produite";
                this.notifyIcon1.BalloonTipText = "WAGGO module avec ip=" + Lmd[2].ip + " non connecté";
                this.notifyIcon1.ShowBalloonTip(0);
            }


            //quatrième waggo
            if (Lmm[3].Connected)
            {
                try
                {
                //    Lmm[3].Connect();

                    for (int k = 0; k < 2; k++)
                    {
                        regResult = Lmm[3].ReadHoldingRegisters
                       (
                       this.slave_id,
                       (ushort)(2 * k + 1),
                       1);

                        enreg[11 + k] = regResult[0];
                    }
                    //modifaication des registres de status //initialization des compteurs

                    for (int k = 0; k < 4; k++)
                    {
                        Lmm[3].WriteSingleRegister(this.slave_id, (ushort)(2 * k), 0x20);
                        Thread.Sleep(5);
                        Lmm[3].WriteSingleRegister(this.slave_id, (ushort)(2 * k + 1), 0);//reset counting
                        Thread.Sleep(5);
                        Lmm[3].WriteSingleRegister(this.slave_id, (ushort)(2 * k), 0);//libérer le comptage
                        Thread.Sleep(5);
                    }
                  //  Lmm[3].Disconnect();

                }
                catch (Exception exe)
                {
                    this.notifyIcon1.BalloonTipIcon = ToolTipIcon.Error;
                    this.notifyIcon1.BalloonTipTitle = "erreur produite";
                    this.notifyIcon1.BalloonTipText = "WAGGO module avec ip=" + Lmd[3].ip + " non connecté";
                    this.notifyIcon1.ShowBalloonTip(0);
                }



            }
            else
            {
                this.notifyIcon1.BalloonTipIcon = ToolTipIcon.Error;
                this.notifyIcon1.BalloonTipTitle = "erreur produite";
                this.notifyIcon1.BalloonTipText = "WAGGO module avec ip=" + Lmd[3].ip + " non connecté";
                this.notifyIcon1.ShowBalloonTip(0);
            }

            //cinquème waggo
            if (Lmm[4].Connected)
            {
                try
                {
                    //Lmm[4].Connect();

                    for (int k = 0; k < 4; k++)
                    {
                        regResult = Lmm[4].ReadHoldingRegisters
                       (
                       this.slave_id,
                       (ushort)(2 * k + 1),
                       1);

                        enreg[13 + k] = regResult[0];
                    }
                    //modifaication des registres de status //initialization des compteurs

                    for (int k = 0; k < 4; k++)
                    {
                        Lmm[4].WriteSingleRegister(this.slave_id, (ushort)(2 * k), 0x20);
                        Thread.Sleep(5);
                        Lmm[4].WriteSingleRegister(this.slave_id, (ushort)(2 * k + 1), 0);//reset counting
                        Thread.Sleep(5);
                        Lmm[4].WriteSingleRegister(this.slave_id, (ushort)(2 * k), 0);//libérer le comptage
                        Thread.Sleep(5);
                    }
                //    Lmm[4].Disconnect();

                }
                catch (Exception exe)
                {
                    this.notifyIcon1.BalloonTipIcon = ToolTipIcon.Error;
                    this.notifyIcon1.BalloonTipTitle = "erreur produite";
                    this.notifyIcon1.BalloonTipText = "WAGGO module avec ip=" + Lmd[4].ip + " non connecté";
                    this.notifyIcon1.ShowBalloonTip(0);
                }


            
            }
            else
            {
                this.notifyIcon1.BalloonTipIcon = ToolTipIcon.Error;
                this.notifyIcon1.BalloonTipTitle = "erreur produite";
                this.notifyIcon1.BalloonTipText = "WAGGO module avec ip=" + Lmd[4].ip + " non connecté";
                this.notifyIcon1.ShowBalloonTip(0);
            }



            /*save data on the database*/
            try
            {

                Enregistrer.enreg_val(enreg, DateTime.Now);
            }
            catch (Exception exe) { }





            string h = "";
            for (int k = 0; k < 17; k++)
                h = h + " " + enreg[k].ToString();
            this.richTextBox1.Text = h;
        }





        private void SetText(string text)
        {
            this.richTextBox1.Text = "  " + (text).ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            timer1.Start();

            button1.Visible = false;

        }

        private void button2_Click(object sender, EventArgs e)
        {
            authentification auth = new authentification();
            auth.ShowDialog();

            // timer1.Stop();
            // Application.Exit();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //load modbus config
            //Waggo_Reader.ModbusIPConfigurator.MODBUSTCP md = ModbusIPConfigurator.getconfig();
            
            Lmd = ModbusIPConfigurator.get_All_config();
            this.txtBx_slaveIP.Text = Lmd[0].ip;
            this.txtBx_Port.Text = Lmd[0].port.ToString();
            this.txtBx_Timeout.Text = Lmd[0].timeout.ToString();
            this.TxtBx_ModbusId.Text = Lmd[0].slaveID.ToString();
            //Initialisation des modbus clients
            for(int h=0;h<Lmd.Count;h++)
            {
                Lmm.Add(new ModbusMasterTCP(Lmd[h].ip, Lmd[h].port));
            }

            mm = new ModbusMasterTCP(Lmd[0].ip, Lmd[0].port);
            mm.RxTimeout = Lmd[0].timeout;
            slave_id = (byte)Lmd[0].slaveID;
            ModbusIPConfigurator.MODBUSTCP mtest = new ModbusIPConfigurator.MODBUSTCP();
            try
            {
                //connect waggo1
                try
                {
                    Lmm[0].Connect();
                    mtest = Lmd[0];
                    mtest.isconnected = true;
                    Lmd[0]= mtest;
                }
                catch(Exception ex)
                {
                    mtest = Lmd[0];
                    mtest.isconnected = false;
                    Lmd[0] = mtest;
                }

                //connect waggo 2
                try
                {
                    Lmm[1].Connect();
                    mtest = Lmd[1];
                    mtest.isconnected = true;
                    Lmd[1] = mtest;
                   
                }
                catch (Exception ex)
                {
                    mtest = Lmd[1];
                    mtest.isconnected = false;
                    Lmd[1] = mtest;
                    
                }


                //connect waggo 3
                try
                {
                    Lmm[2].Connect();
                    mtest = Lmd[2];
                    mtest.isconnected = true;
                    Lmd[2] = mtest;
                }
                catch (Exception ex)
                {
                    mtest = Lmd[2];
                    mtest.isconnected = false;
                    Lmd[2] = mtest;
                }
                //connect waggo 4
                try
                {
                    Lmm[3].Connect();
                    mtest = Lmd[3];
                    mtest.isconnected = true;
                    Lmd[3] = mtest;
                }
                catch (Exception ex)
                {
                    mtest = Lmd[3];
                    mtest.isconnected = false;
                    Lmd[3] = mtest;
                }


                //connect waggo 5
                try
                {
                    Lmm[4].Connect();
                    mtest = Lmd[4];
                    mtest.isconnected = true;
                    Lmd[4] = mtest;
                }
                catch (Exception ex)
                {
                    mtest = Lmd[4];
                    mtest.isconnected = false;
                    Lmd[4] = mtest;
                }
                //Lmm[1].Connect();
                /*Lmm[2].Connect();
                Lmm[3].Connect();
                Lmm[4].Connect();*/
            }
            catch (Exception exe)
            {
                MessageBox.Show(exe.ToString());
            }
#if RS485
            try
            {
                COM1.Open();
                byte[] b = { 0x25 };
                for (int i = 0; i < 9; i++)
                {
                    b[0]++;
                    COM1.Write(b, 0, 1);
                    Thread.Sleep(200);
                    string str = COM1.ReadExisting();
                }
            }
            catch (Exception exe) { }
            COM1.Close();
            Thread.Sleep(1000);


            try
            {
                COM2.Open();
                byte[] b = { 0x2E };
                for (int i = 0; i < 8; i++)
                {
                    b[0]++;
                    COM2.Write(b, 0, 1);
                    Thread.Sleep(200);
                    string str = COM2.ReadExisting();
                }
            }
            catch (Exception exe) { }
            COM2.Close();
#endif

            this.ShowInTaskbar = false;
            this.notifyIcon1.Icon = this.Icon;
            button1_Click(sender, e);
        }

        private void notifyIcon1_DoubleClick(object sender, EventArgs e)
        {
            this.ShowInTaskbar = true;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            ModbusIPConfigurator.MODBUSTCP mdd =  ModbusIPConfigurator.GetConfigByName(this.comboBox1.SelectedItem.ToString());

            this.txtBx_slaveIP.Text = mdd.ip;
            this.txtBx_Port.Text = mdd.port.ToString();
            this.txtBx_Timeout.Text = mdd.timeout.ToString();
            this.TxtBx_ModbusId.Text = mdd.slaveID.ToString();

            if (Lmm[this.comboBox1.SelectedIndex].Connected)
                button3.Text = "Déconnecter";
            else
                button3.Text = "Connecter";


        }

        private void button3_Click(object sender, EventArgs e)
        {
            if(button3.Text=="Connecter")
            {
                try
                {
                    Lmm[this.comboBox1.SelectedIndex].Connect();
                    button3.Text = "Déconnecter";
                }catch(Exception ex)
                {
                    MessageBox.Show("impossible de se connecter à l'hote " + Lmd[this.comboBox1.SelectedIndex].ip);
                }
            }
            else if (button3.Text == "Déconnecter")
            {
                DialogResult result = MessageBox.Show("vous allez déconnecter le Module Waggo /n continuer? ", "Confirmation", MessageBoxButtons.YesNoCancel);
                if (result == DialogResult.Yes)
                {
                    Lmm[this.comboBox1.SelectedIndex].Disconnect();
                    button3.Text = "Connecter";
                }
                else if (result == DialogResult.No)
                {
                    
                }
               
               
            }
            else
            {
                MessageBox.Show("veuillez sélectionner Un Module");
            }
        }

        private void btn_validate_Click(object sender, EventArgs e)
        {
            ModbusIPConfigurator.UpdateModbusConf(this.txtBx_slaveIP.Text, Convert.ToInt32(this.txtBx_Port.Text)
                , Convert.ToInt32(this.txtBx_Timeout.Text), Convert.ToInt32(this.TxtBx_ModbusId.Text));

            mm = new ModbusMasterTCP(this.txtBx_slaveIP.Text, Convert.ToInt32(this.txtBx_Port.Text));
            this.slave_id = (byte)Convert.ToInt32(this.TxtBx_ModbusId.Text);
            mm.RxTimeout = Convert.ToInt32(this.txtBx_Timeout.Text);
            try
            {
               // mm.Connect();

            }
            catch (Exception exe)
            {
                this.notifyIcon1.BalloonTipIcon = ToolTipIcon.Error;
                this.notifyIcon1.BalloonTipTitle = "erreur produite";
                this.notifyIcon1.BalloonTipText = "cànnexion impossible au module spécifié";
                this.notifyIcon1.ShowBalloonTip(0);
            }
        }
    }
}
