﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Waggo_Reader
{
    class ModbusIPConfigurator
    {
        public struct MODBUSTCP
        {
            public string ip;
            public int port;
            public int timeout;
            public int slaveID;
            public string Waggo_Name;
            public Boolean isconnected;
        }

        public ModbusIPConfigurator()
        {

        }

        /*get the last MODBUS CONFIG FROM THE DATABASES*/
        public static MODBUSTCP getconfig()
        {
            List<MODBUSTCP> mODBUSTCPs = new List<MODBUSTCP>();
            MODBUSTCP md = new MODBUSTCP();
            MySqlConnection con = DBconnect.con1();
            string query = "select * from WAGGO";
            MySqlCommand cmd = new MySqlCommand(query, con);
            try
            {
                con.Open();
            }
            catch (Exception exe)
            {
            }
            MySqlDataReader rd = cmd.ExecuteReader();
            while (rd.Read())
            {
                md.ip = rd["IPaddress"].ToString();
                md.port = rd.GetInt32(1);
                md.timeout = rd.GetInt32(2);
                md.slaveID = rd.GetInt32(3);
                mODBUSTCPs.Add(md);
            }
            rd.Close();
            con.Close();

            return md;
        }

        /*get All the Waggo Config froim the Database*/
        public static List<MODBUSTCP> get_All_config()
        {
          
            List<MODBUSTCP> mODBUSTCPs = new List<MODBUSTCP>();
            MODBUSTCP md = new MODBUSTCP();
            MySqlConnection con = DBconnect.con1();
            try
            {
                string query = "select * from WAGGO";
                MySqlCommand cmd = new MySqlCommand(query, con);
                try
                {
                    con.Open();
                }
                catch (Exception exe)
                {
                }
                MySqlDataReader rd = cmd.ExecuteReader();
                while (rd.Read())
                {
                    md.ip = rd["IPaddress"].ToString();
                    md.port = rd.GetInt32(1);
                    md.timeout = rd.GetInt32(2);
                    md.slaveID = rd.GetInt32(3);
                    md.Waggo_Name = rd["Waggo_Name"].ToString();
                    

                    mODBUSTCPs.Add(md);
                }
                rd.Close();
                con.Close();
            }catch(Exception e) { MessageBox.Show("Exception1= "+ e.ToString()); }

            return mODBUSTCPs;
        }

        public static MODBUSTCP GetConfigByName(String Name)
        {
            MODBUSTCP md = new MODBUSTCP();
            MySqlConnection con = DBconnect.con1();
            try
            {
                string query = "select * from WAGGO where Waggo_Name="+"'"+ Name + "'";
                MySqlCommand cmd = new MySqlCommand(query, con);
                try
                {
                    con.Open();
                }
                catch (Exception exe)
                {
                    MessageBox.Show("Exception2= " + exe.ToString());
                }
                MySqlDataReader rd = cmd.ExecuteReader();
                if (rd.Read())
                {
                    md.ip = rd["IPaddress"].ToString();
                    md.port = rd.GetInt32(1);
                    md.timeout = rd.GetInt32(2);
                    md.slaveID = rd.GetInt32(3);
                    //md.Waggo_Name = rd["Waggo_Name"].ToString();

                    
                }
                rd.Close();
                con.Close();
            }
            catch (Exception e) { MessageBox.Show("Exception3= " + e.ToString()); }
            return md;
        }

        public static void UpdateModbusConf(string ip, int port, int timeout, int slave_id)
        {
            MySqlConnection con = DBconnect.con1();
            string query = "update WAGGO set IPaddress='" + ip + "',port=" + port + ",Timeout=" + timeout + ",slave_id=" + slave_id + "";
            MySqlCommand cmd = new MySqlCommand(query, con);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();

        }
    }
}
