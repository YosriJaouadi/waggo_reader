﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Waggo_Reader
{
    static class Program
    {
        static Mutex mutex;
        /// <summary>
        /// Point d'entrée principal de l'application.
        /// </summary>
        [STAThread]
        static void Main()

        {
            string application_name = Path.GetFileName(Application.ExecutablePath);
            try
            {
                Program.mutex = Mutex.OpenExisting(application_name);
                MessageBox.Show(String.Format("L'application {0} est déjà lancée.", application_name), "Avertissement", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch
            {
                Program.mutex = new Mutex(true, application_name);
                try
                {



                    Application.Run(new Form1());

                }
                catch (Exception exe) { MessageBox.Show(exe.ToString()); }
            }
        }
    } 
}
