﻿namespace Waggo_Reader
{
    partial class authentification
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Log_text = new System.Windows.Forms.TextBox();
            this.password_text = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.OK_bt = new System.Windows.Forms.Button();
            this.quit_bt = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Log_text
            // 
            this.Log_text.Location = new System.Drawing.Point(141, 52);
            this.Log_text.Name = "Log_text";
            this.Log_text.Size = new System.Drawing.Size(100, 20);
            this.Log_text.TabIndex = 0;
            // 
            // password_text
            // 
            this.password_text.Location = new System.Drawing.Point(141, 104);
            this.password_text.Name = "password_text";
            this.password_text.Size = new System.Drawing.Size(100, 20);
            this.password_text.TabIndex = 1;
            this.password_text.UseSystemPasswordChar = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(69, 59);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Login";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(69, 111);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Password";
            // 
            // OK_bt
            // 
            this.OK_bt.Location = new System.Drawing.Point(82, 151);
            this.OK_bt.Name = "OK_bt";
            this.OK_bt.Size = new System.Drawing.Size(65, 23);
            this.OK_bt.TabIndex = 4;
            this.OK_bt.Text = "OK";
            this.OK_bt.UseVisualStyleBackColor = true;
            this.OK_bt.Click += new System.EventHandler(this.OK_bt_Click);
            // 
            // quit_bt
            // 
            this.quit_bt.Location = new System.Drawing.Point(166, 151);
            this.quit_bt.Name = "quit_bt";
            this.quit_bt.Size = new System.Drawing.Size(64, 23);
            this.quit_bt.TabIndex = 5;
            this.quit_bt.Text = "quitter";
            this.quit_bt.UseVisualStyleBackColor = true;
            this.quit_bt.Click += new System.EventHandler(this.quit_bt_Click);
            // 
            // authentification
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(312, 197);
            this.Controls.Add(this.quit_bt);
            this.Controls.Add(this.OK_bt);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.password_text);
            this.Controls.Add(this.Log_text);
            this.Name = "authentification";
            this.Text = "authentification";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox Log_text;
        private System.Windows.Forms.TextBox password_text;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button OK_bt;
        private System.Windows.Forms.Button quit_bt;
    }
}