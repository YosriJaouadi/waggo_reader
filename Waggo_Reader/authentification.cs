﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Waggo_Reader
{
    public partial class authentification : Form
    {
        public authentification()
        {
            InitializeComponent();
        }

        private void OK_bt_Click(object sender, EventArgs e)
        {
            authentif auth = new authentif();

            if (auth.is_admin(this.Log_text.Text, this.password_text.Text))
            {
                Application.Exit();
            }
            else
            {
                MessageBox.Show("consulter l'administrateur");
                this.Close();
            }
        }

        private void quit_bt_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
